package com.liuzhao.packet.server;

/**
 * @author liuxianzhao
 * @ClassName ServerMainApplication
 * @Description 服务端主程序入口
 * @date 2017年09月27日 13:44
 */
public class ServerMainApplication {

    public static void main(String[] args) {
        NettyServerStartup.newInstance().start();
    }

}
