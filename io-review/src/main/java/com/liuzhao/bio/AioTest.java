package com.liuzhao.bio;

import org.junit.Test;

/**
 * @author liuxianzhao
 * @ClassName AioTest
 * @Description 阻塞型IO测试类
 * @date 2017年08月22日 07:19
 */
public class AioTest {


    @Test
    public void startServer() {
        MySocketServer mss = new MySocketServer(28080);
        mss.start();
    }

    @Test
    public void startClient() {
        MyServer cilent = new MyServer();
        cilent.start();
    }
}
