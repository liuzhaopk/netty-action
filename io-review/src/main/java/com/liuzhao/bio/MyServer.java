package com.liuzhao.bio;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author liuxianzhao
 * @ClassName MyServer
 * @Description 阻塞型IO客户端
 * @date 2017年08月22日 07:06
 */
public class MyServer {
    private String address = "127.0.0.1";
    private int protNumber = 28080;

    public MyServer() {

    }

    public MyServer(String address, int protNumber) {
        this.address = address;
        this.protNumber = protNumber;
    }

    public void start() {
        try (
                Socket socket = new Socket(address, protNumber);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        ) {
            //向服务器端发送数据
            out.println("你好");
            //阻塞
            String response = in.readLine();
            System.out.println("Client: " + response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}