package com.cn.server.module.chat.handler;
import com.liuzhao.common.core.annotion.SocketCommand;
import com.liuzhao.common.core.annotion.SocketModule;
import com.liuzhao.common.core.model.Result;
import com.liuzhao.common.module.ModuleId;
import com.liuzhao.common.module.chat.ChatCmd;
import com.liuzhao.common.module.chat.request.PrivateChatRequest;
import com.liuzhao.common.module.chat.request.PublicChatRequest;
/**
 * 聊天
 * @author liuxianzhao
 *
 */
@SocketModule(module = ModuleId.CHAT)
public interface ChatHandler {
	
	
	/**
	 * 广播消息
	 * @param playerId
	 * @param data {@link PublicChatRequest}
	 * @return
	 */
	@SocketCommand(cmd = ChatCmd.PUBLIC_CHAT)
	public Result<?> publicChat(long playerId, byte[] data);
	
	
	
	/**
	 * 私人消息
	 * @param playerId
	 * @param data {@link PrivateChatRequest}
	 * @return
	 */
	@SocketCommand(cmd = ChatCmd.PRIVATE_CHAT)
	public Result<?> privateChat(long playerId, byte[] data);
}
