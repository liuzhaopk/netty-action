package com.cn.server.module.player.handler;

import com.liuzhao.common.core.annotion.SocketCommand;
import com.liuzhao.common.core.annotion.SocketModule;
import com.liuzhao.common.core.model.Result;
import com.liuzhao.common.core.session.Session;
import com.cn.common.module.ModuleId;
import com.liuzhao.common.module.player.PlayerCmd;
import com.liuzhao.common.module.player.request.LoginRequest;
import com.liuzhao.common.module.player.request.RegisterRequest;
import com.liuzhao.common.module.player.response.PlayerResponse;
/**
 * 玩家模块
 * @author liuxianzhao
 *
 */
@SocketModule(module = ModuleId.PLAYER)
public interface PlayerHandler {
	
	
	/**
	 * 创建并登录帐号
	 * @param session
	 * @param data {@link RegisterRequest}
	 * @return
	 */
	@SocketCommand(cmd = PlayerCmd.REGISTER_AND_LOGIN)
	public Result<PlayerResponse> registerAndLogin(Session session, byte[] data);
	

	/**
	 * 登录帐号
	 * @param session
	 * @param data {@link LoginRequest}
	 * @return
	 */
	@SocketCommand(cmd = PlayerCmd.LOGIN)
	public Result<PlayerResponse> login(Session session, byte[] data);

}
