package com.liuzhao.clients;

import com.liuzhao.clients.core.NettyClientStartup;

/**
 * @author liuxianzhao
 * @ClassName ClientMainApplication
 * @Description 客户端主程序入口
 * @date 2017年09月27日 14:25
 */
public class ClientMainApplication {
    public static void main(String[] args) {
        NettyClientStartup.newInstance().start();
    }
}
